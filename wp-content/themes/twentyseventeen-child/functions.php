<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 30);
function theme_enqueue_styles() {
    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style, '')
    );

    wp_enqueue_script( 'popup', get_template_directory_uri() . '-child/assets/js/popup.js', array ( 'jquery' ), 1.4, true);
	wp_enqueue_script( 'custom', get_template_directory_uri() . '-child/assets/js/custom.js', array ( 'jquery' ), 1.4, true);
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15);

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 40 );
