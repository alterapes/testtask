<?php
/**
 * Displays footer site contacts
 */
?>
<div class="contact-section">
	<div class="contact-section-row">
		<a class="button-only-phones" href="tel:0987654321">Call Us<span>: 0987654321</span></a>
	</div>
	<div class="contact-section-row">
		<a class="button-only-phones" href="mailto:testdomain@mail.to">Email<span>: testdomain@mail.to</span></a>
	</div>
	<div class="contact-section-row">
		<a href="#popup-content" class="button open-popup-link">
			Contact Us
		</a>
	</div>
	<div id="popup-content" class="mfp-hide popup">
		<div class="popup-wrapper">
			<div class="popup-content">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac suscipit ipsum. Vivamus tristique, libero ac mollis ultrices, libero augue convallis lorem, a tristique orci tellus at tortor. Quisque vel nunc bibendum, mollis tortor id, fringilla ex. Nunc mattis metus tellus, et consequat enim tristique a. Nullam interdum tincidunt nisl, ut efficitur ligula molestie et. Fusce eleifend, arcu in hendrerit suscipit, quam ipsum congue enim, vel rhoncus ante risus vel lectus. Etiam maximus ornare sollicitudin. Nunc at pharetra velit. Integer non sapien ut ante elementum gravida. Integer imperdiet suscipit orci, nec hendrerit arcu lobortis quis. Vestibulum pharetra tincidunt nunc nec aliquet.
				</p>
			</div>
		</div>
	</div>

</div><!-- .site-contacts -->