<?php
/**
 * Displays footer site info
 */
?>

<a class="info-link" href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>">
	<?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?>
</a>
